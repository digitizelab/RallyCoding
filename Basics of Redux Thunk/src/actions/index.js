import axios from 'axios';

export function fetchUsers() {
  const request = axios.get('http://jsonplaceholder.typicode.com/users');

    // Usually action creators returns plain Javascript object, redux-thunk allows to return a function
    // Inside the function it waits till the data is resolved
  return (dispatch) => {
    request.then(({data}) => {
      dispatch({ type: 'FETCH_PROFILES', payload: data })
    });
  };
}
